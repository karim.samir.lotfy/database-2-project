import java.util.ArrayList;
import java.util.Arrays;

import javax.naming.spi.DirStateFactory.Result;

public class ndArray {
    int[] dim;
    Object[] content;
    ndArray(int[] dim){
        this.dim = dim;
        int max_pos = 1;
        for (int i : dim) {
            max_pos *= i;
        }
        this.content = new Object[max_pos];
    }

    public static void main(String[] args) {
        // the point is to create a function that when given coordinates to a tensor can get its value easly
        int[][][][] arr = new int[10][10][10][10];
        ndArray my_arr = new ndArray(new int[]{10,10,10, 10}); // create array with 10 rows, 10 cols, 10 values in each row/col create 3d tensor with dim 10,10,10
        int count = 0;
        for (int k = 0; k < 10; k++) {
            for(int i= 0; i < 10; i++){
                for (int j = 0; j < 10; j++) {
                    for (int j2 = 0; j2 < 10; j2++) {
                        arr[k][i][j][j2] = count;
                        my_arr.set(new int[]{k, i, j, j2}, count);
                        count++;
                    }
                }
            }
        }
        
        ArrayList<Object> a = my_arr.get_set(new int[]{0, 2, -1, 0});
            for (Object object : a) {
                System.out.println(a);
            }
    }
   
  

   
    ArrayList<Object> get_set(int[] index){
        /*
        index = [1, 2, -1, 5, -1]
            <[[1],[2],[0,1,2,3,4,5, 6, 7, 8, 9],[5],[[0,1,2,3,4,5, 6, 7, 8, 9]]]>
         */
        
        ArrayList<int[]> loop_index = new ArrayList<int []>();
        
        for (int i = 0; i < index.length; i++) {
            int index_value = index[i];
            if(index_value == -1){
                // create all the premutaions of this index like of the max of xol is 5 then we need to loop on 0,1,2,3,4
                int max_size_of_this_dim = this.dim[i];
                int[] premutaions = new int[max_size_of_this_dim];
                for(int j =0; j < max_size_of_this_dim; j++){
                    premutaions[j] = j;
                }
                loop_index.add(premutaions);
            }else {
                loop_index.add(new int[]{index_value});
            }

           
        }
        

        ArrayList<ArrayList<Integer>> indecies = getPremutaions(0, loop_index);
        ArrayList<Object> results = new ArrayList<>();
        for (ArrayList<Integer> position_index : indecies) {
            int[] position_index_in_array_form = position_index.stream().mapToInt(i -> i).toArray();//new int[position_index.size()];
           // System.arraycopy(position_index, 0, position_index_in_array_form, 0, position_index.size()); // method equivilent to int[] = position_index.toArrays() but this doesn work cuz java we keda
            results.add(this.get(position_index_in_array_form));
        }
        return results;
    }
    Object get(int[] index){
        int pos = get_1d_pos(index);
        return this.content[pos];
    }

    void set(int[] index, Object inserted_value){
        int pos = this.get_1d_pos(index);
        this.content[pos] = inserted_value;
    }

    int get_1d_pos(int [] index){
        
        int pos = 0;
        for (int c = 0; c < index.length; c++) {
            int index_value = index[c];
            int acc = 1; // multilply by the max size for each dimenstion
            for(int starting_dim = 1+c; starting_dim < this.dim.length; starting_dim++){
                acc *= this.dim[starting_dim];
            }
            acc*= index_value; // multply by index-value to offset based on dims
            pos += acc;
        }

        return pos;
    }

    static ArrayList<ArrayList<Integer>> getPremutaions(int depth,  ArrayList<int[]> index){
        if(depth == index.size()-1){
            ArrayList<ArrayList<Integer>> temp = new ArrayList<>();
            
                for (int e : index.get(depth)) {
                    ArrayList<Integer> temp2 = new ArrayList<>();
                    temp2.add(e);
                    temp.add(temp2);
                }
            
            return temp;
        }

        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        for (int element : index.get(depth)) {
            ArrayList<ArrayList<Integer>> old = getPremutaions(depth+1, index);
            for (ArrayList<Integer> arrayList : old) {
                arrayList.add(0, element);
                res.add(arrayList);
            }
        }
        return res;
    }
}
