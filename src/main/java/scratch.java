import java.util.ArrayList;
import java.util.Arrays;

public class scratch {
    public static void main(String[] args) {
        ArrayList<int[]> index = new ArrayList<>();
        index.add(new int[]{0, 1, 2});
        index.add(new int[]{0, 1, 2});
        index.add(new int[]{0, 2});
        index.add(new int[]{2});
        for (ArrayList<Integer> is : getPremutaions(0, index)) {
            System.out.println(Arrays.toString(is.toArray()));
        }
    }
    static ArrayList<ArrayList<Integer>> getPremutaions(int depth,  ArrayList<int[]> index){
        if(depth == index.size()-1){
            ArrayList<ArrayList<Integer>> temp = new ArrayList<>();
            
                for (int e : index.get(depth)) {
                    ArrayList<Integer> temp2 = new ArrayList<>();
                    temp2.add(e);
                    temp.add(temp2);
                }
            
            return temp;
        }

        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        for (int element : index.get(depth)) {
            ArrayList<ArrayList<Integer>> old = getPremutaions(depth+1, index);
            for (ArrayList<Integer> arrayList : old) {
                arrayList.add(0, element);
                res.add(arrayList);
            }
        }
        return res;
    }
}
