import java.io.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Vector;

public class Page implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -1111554846970979289L;
	Vector<Hashtable<String,Object>> content ;
    private Comparable max=0;
    private Comparable min=0;
    private int maxsize;
    transient Page nextpage=null;
    transient Table parent = null;
    String parentName;
	int ID;
	int overflow = 0;

    public Comparable getmax(){
        return max;
    }
    public Comparable getmin(){
        return min;
    }
    public void setmax(Comparable max){
        this.max = max;
    }
    public void setmin(Comparable min){
        this.min = min;
    }
    public Vector<Hashtable<String, Object>> getcontent(){
    	return content;
    }


    public void updatePage(){
        String path = SerializePage();
        Hashtable<String,Object> record = new Hashtable<>();
        record.put("min",min);
        record.put("max",max);
        record.put("path",path);
        try {
            parent.pages.get(ID).set(overflow,record);
		} catch (Exception e) {
			parent.pages.get(ID).insertElementAt(record, overflow);
		}
    }
    public Page() throws IOException { 

        FileInputStream fis = new FileInputStream("src/main/resources/DBApp.config");
        Properties props = new Properties();
        props.load(fis);
        int pagemax = Integer.parseInt(props.getProperty("MaximumRowsCountinPage"));
        content= new Vector<>(pagemax);
        maxsize=pagemax;
        ID = parent.IdGenerator++;
        updatePage();
    }
    public Page(Hashtable<String,Object> record) throws IOException {
        FileInputStream fis = new FileInputStream("src/main/resources/DBApp.config");
        Properties props = new Properties();
        props.load(fis);
        int pagemax = Integer.parseInt(props.getProperty("MaximumRowsCountinPage"));
        content= new Vector<>(pagemax);
        maxsize=pagemax;
        content.add(record);
        min = (Comparable) record.get(parent.clusteringKey);
        max = (Comparable) record.get(parent.clusteringKey);
        ID = parent.IdGenerator++;
        updatePage();
        
    }
    public Page(Hashtable<String,Object> record, Table parent) throws IOException { 
        FileInputStream fis = new FileInputStream("src/main/resources/DBApp.config");
        Properties props = new Properties();
        props.load(fis);
        int pagemax = Integer.parseInt(props.getProperty("MaximumRowsCountinPage"));
        content= new Vector<>(pagemax);
        maxsize=pagemax;
        content.add(record);
        this.parent=parent;
        parentName = parent.tableName;
        min = (Comparable) record.get(parent.clusteringKey);
        max = (Comparable) record.get(parent.clusteringKey);
        ID = parent.IdGenerator++;
        updatePage();
    }
    
    private Page(Hashtable<String,Object> record, Table parent, int overflow, int ID) throws IOException { 
        FileInputStream fis = new FileInputStream("src/main/resources/DBApp.config");
        Properties props = new Properties();
        props.load(fis);
        int pagemax = Integer.parseInt(props.getProperty("MaximumRowsCountinPage"));
        content= new Vector<>(pagemax);
        maxsize=pagemax;
        content.add(record);
        this.parent=parent;
        parentName = parent.tableName;
        min = (Comparable) record.get(parent.clusteringKey);
        max = (Comparable) record.get(parent.clusteringKey);
        this.overflow = overflow;
        this.ID = ID;
        updatePage();
    }

    public boolean isFull(){
        return content.size()==maxsize;
    }
    public boolean isEmpty(){
        return content.size()==0;
    }

    public Hashtable<String,Object> get(){
        return content.get(0);
    }


    public void insert(Hashtable<String,Object> record) throws IOException {
        Hashtable<String,Object> record2 = (Hashtable<String, Object>) record.clone();

        if (isFull()) {
        	int of = 1;
        	try {
        	while(DeserializePage(this.parent.pages.get(ID).get(of).get("path").toString(), this.parent).isFull()) {
        		of++;
        	}
        	//of++;
        	Page pof = DeserializePage(this.parent.pages.get(ID).get(of).get("path").toString(), this.parent);
        	pof.insert(record2);
        	} catch(ArrayIndexOutOfBoundsException e) {
        		Page pof2 = new Page(record2, this.parent, of, ID);
        	} finally {
				return;
			}
        }
        	
        	
//            Hashtable<String, Object> extra = content.remove(content.size() - 1);
//            
//            try {
//            parent.pages.get(ID+1);
//            if(Page.DeserializePage(parent.pages.get(this.ID+1).firstElement().get("path").toString(), parent) == null) {
//                nextpage = new Page(extra,parent);
//            }
//            else
//            	Page.DeserializePage(parent.pages.get(this.ID+1).firstElement().get("path").toString(), this.parent).insert(extra);
//            } catch (IndexOutOfBoundsException e) {
//            	nextpage = new Page(extra,parent);
//            }
//        }
        int size = content.size();
        Comparable id = (Comparable) record2.get(parent.clusteringKey);
        boolean inserted=false;
        for (int i = 0; i < size; i++) {
        	Comparable x = (Comparable) content.get(i).get(parent.clusteringKey);
            if (x.compareTo(id) < 0) continue;
            content.add(i, record2);
            inserted=true;
            break;
        }
        if (!inserted)
            content.add(record2);
        
        min = (Comparable) content.firstElement().get(parent.clusteringKey);
        max = (Comparable) content.lastElement().get(parent.clusteringKey);

        updatePage();

    }

    public void show(Comparable id) throws IOException, DBAppException {
        Hashtable<String,Object> result;
        try {
            result = this.content.get(this.search(id));
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new DBAppException("problem here");
        }
        for(Entry<String, Object> x: result.entrySet()) {
            System.out.println(x);
        }
        System.out.println();
    }

    public int search(Comparable id) throws IOException {
        int size = content.size();
        int lowIndex = 0;
        int highIndex=size-1;
        int elementpos=0;
        Comparable temp2 =  (Comparable)(content.firstElement().get(parent.clusteringKey));
        if(temp2.compareTo(id)==0)
            return 0;
        while (lowIndex <= highIndex) {
            int midIndex = (lowIndex + highIndex) / 2;
            Comparable temp =  (Comparable)(content.get(midIndex).get(parent.clusteringKey));
            if (temp.compareTo(id)==0) {
                elementpos = midIndex;
                return elementpos;
            } else if (temp.compareTo(id)>0) { 
                highIndex = midIndex-1;
            } else if (temp.compareTo(id)<0) {
                lowIndex = midIndex+1;
            }
        }
        return -1;
    }

    public void print(){
        System.out.println(content);
        if (nextpage!=null)
            nextpage.print();
    }

    public boolean delete(Comparable id2) throws IOException {
        int pos = search(id2);
        boolean flag = false;
        if (pos==-1)
            return false;
        else{
            if (isFull() && nextpage!=null){
                flag=true;
            }
            content.remove(pos);
        }
        if(!content.isEmpty()) {
            min = (Comparable) content.firstElement().get(parent.clusteringKey);
            max = (Comparable) content.lastElement().get(parent.clusteringKey);
        }
        updatePage();
        return true;

    }

    public boolean update2(Comparable id2,Hashtable<String,Object> newrecord, int overflow) throws IOException {
        int pos = search(id2);
        if (pos==-1) {
        		int of = overflow;
            	try {
            		while(!DeserializePage(this.parent.pages.get(ID).get(of).get("path").toString(), this.parent).update2(id2, newrecord, ++of)) {
            			//of++;
            		}
            		return true;
            	} catch(ArrayIndexOutOfBoundsException e) {
                    return false;
            	}
        	}
        else{
            Hashtable<String,Object> merged = merge(this.content.get(pos), newrecord);
            content.set(pos,merged);
            updatePage();
            return true;
        }
    }

    public boolean update(Comparable id2,Hashtable<String,Object> newrecord) throws IOException {
        int pos = search(id2);
        if (pos==-1) {
        		int of = 1;
            	try {
            		while(!DeserializePage(this.parent.pages.get(ID).get(of).get("path").toString(), this.parent).update2(id2, newrecord, ++of)) {
            			//of++;
            		}
            		return true;
            	} catch(ArrayIndexOutOfBoundsException e) {
                    return false;
            	}
        	}
        else{
            Hashtable<String,Object> merged = merge(this.content.get(pos), newrecord);
            content.set(pos,merged);
            updatePage();
            return true;
        }
    }

    public static Hashtable<String,Object> merge(Hashtable<String,Object> o, Hashtable<String,Object> n){
        Hashtable<String,Object> merged = (Hashtable<String, Object>) o.clone();

        ArrayList<String> keys = new ArrayList<String>();
        ArrayList<Object> values = new ArrayList<Object>();
        for(String x: o.keySet()) {
            keys.add(x);
            values.add((n.get(x) == null)? o.get(x):n.get(x));
        }

        int i=0;
        for(String x: keys) {
            merged.put(keys.get(i), values.get(i));
            i++;
        }

        return merged;
    }
    private String SerializePage() {
    	String path;
    	if(this.overflow == 0)
    		path = "./src/main/resources/data/"+parent.tableName+"_"+ID+".ser";
    	else
    		path = "./src/main/resources/data/"+parent.tableName+"_"+ID+"_O"+overflow+".ser";
    	try {
            FileOutputStream file = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(file);

            out.writeObject(this);
            out.close();
            file.close();

            System.out.println("Page serialized to path:" + path);
            return path;
        }catch(IOException e){
        	if(e.getMessage() != null)
        		System.err.println(e.getMessage());
            //e.printStackTrace();
        }
    return path;
    }
    
    /*public static void main(String[] args) {
    	
    	Page p1 = DeserializePage("./src/main/resources/data/pcs_0.ser", null);
    	System.out.println(p1);
    	System.out.println("===");
    	Page p2 = DeserializePage("./src/main/resources/data/pcs_1.ser", null);
    	System.out.println(p2);
    	System.out.println("===");
    	Page p3 = DeserializePage("./src/main/resources/data/pcs_2.ser", null);
    	System.out.println(p3);
    	System.out.println("===");
    	Page p4 = DeserializePage("./src/main/resources/data/pcs_2_O1.ser", null);
    	System.out.println(p4);
    	System.out.println("===");
    	Page p5 = DeserializePage("./src/main/resources/data/pcs_2_O2.ser", null);
    	System.out.println(p5);
    	System.out.println("===");
    	Page p6 = DeserializePage("./src/main/resources/data/pcs_2_O3.ser", null);
    	System.out.println(p6);
    	System.out.println("===");
    	Page p7 = DeserializePage("./src/main/resources/data/pcs_2_O4.ser", null);
    	System.out.println(p7);
    	System.out.println("===");
    	Page p8 = DeserializePage("./src/main/resources/data/pcs_3.ser", null);
    	System.out.println(p8);
    	System.out.println("===");
    	Page p9 = DeserializePage("./src/main/resources/data/pcs_3_O1.ser", null);
    	System.out.println(p9);
    	System.out.println("===");
    	Page p10 = DeserializePage("./src/main/resources/data/pcs_3_O2.ser", null);
    	System.out.println(p10);
    	System.out.println("===");
//    	p = DeserializePage("./src/main/resources/data/pcs_0_O1.ser", null);
//    	System.out.println(p);
//    	System.out.println("===");
//    	p = DeserializePage("./src/main/resources/data/pcs_0_O2.ser", null);
//    	System.out.println(p);
//    	System.out.println("===");
//    	p = DeserializePage("./src/main/resources/data/pcs_0_O3.ser", null);
//    	System.out.println(p);
//    	System.out.println("===");
//    	p = DeserializePage("./src/main/resources/data/pcs_1.ser", null);
//    	System.out.println(p);
//    	System.out.println("===");
//    	p = DeserializePage("./src/main/resources/data/pcs_2.ser", null);
//    	System.out.println(p);
//    	System.out.println("===");
//    	p = DeserializePage("./src/main/resources/data/pcs_3.ser", null);
//    	System.out.println(p);
//    	System.out.println("===");
    	
//    	Vector<Vector<Integer>> test = new Vector<Vector<Integer>>(0,1);
//    	test.add(new Vector<Integer>(0,1));
//    	test.get(0).add(0);
//    	test.add(new Vector<Integer>(0,1));
//    	test.get(1).add(1);
//    	test.add(new Vector<Integer>(0,1));
//    	test.get(2).add(2);
//    	test.add(new Vector<Integer>(0,1));
//    	test.get(3).add(3);
//    	test.add(new Vector<Integer>(0,1));
//    	test.get(4).add(4);
//    	test.add(new Vector<Integer>(0,1));
//    	test.get(0).add(0);
//    	test.get(0).add(1);
//    	test.get(0).add(2);
//    	test.get(0).add(3);
//    	test.get(0).add(4);

//		Page before = DeserializePage("./src/main/resources/data/5.ser");
//		System.out.println(before);
//		Page after = DeserializePage("./src/main/resources/data/6.ser");
//		System.out.println("======");
//		System.out.println(after);
    	
//		Page x = DeserializePage("./src/main/resources/data/courses_0.ser");
//		System.out.println("======");
//		System.out.println(x);
//		
//		x = DeserializePage("./src/main/resources/data/pcs_0.ser");
//		System.out.println("======");
//		System.out.println(x);
//		
//		x = DeserializePage("./src/main/resources/data/students_0.ser");
//		System.out.println("======");
//		System.out.println(x);
//		
//		x = DeserializePage("./src/main/resources/data/transcripts_0.ser");
//		System.out.println("======");
//		System.out.println(x);

	}*/

    public static Page DeserializePage(String path, Table parent) {

        Page page = null;

        try {

            FileInputStream file = new FileInputStream(path);
            ObjectInputStream in = new ObjectInputStream(file);

            page = (Page)in.readObject();

            in.close();
            file.close();

            System.out.println("Page deserialized!");

        }catch(IOException e){
        	if(e.getMessage() != null)
        		System.err.println(e.getMessage());
            //e.printStackTrace();
        }catch(ClassNotFoundException e){
            System.err.println("Page not found!");
            //e.printStackTrace();
        }
        
        page.parent = parent;

        return page;
    }
    
    public String toString() {
		String str = "";
		for (Hashtable<String, Object> row : this.content) {
			str += row + "\n";
		}
		return str;
	}
}